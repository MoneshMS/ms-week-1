import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  timerSec: number = 1000;

  decrementTimer() {
    this.timerSec -= 10;
  }

  incrementTimer() {
    this.timerSec += 10;
  }
}
